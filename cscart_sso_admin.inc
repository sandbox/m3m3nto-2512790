<?php
/**
 * @file
 * Google CSI module administration menu items.
 *
 */

/**
* Display main option form
*/
function cscart_sso_admin_form(){
  return drupal_get_form('cscart_sso_form');
}

/*
* Returns the rebuild languages form  
*/
function cscart_sso_form($form, &$form_state){
  $form['sso_cookie_base_user_token'] = array(
    '#title' => t('Cookie user token'),
    '#description' => t('Set sso cookie user token defined in cs_cart'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => variable_get('sso_cookie_base_user_token', 'sid_customer_'),
  );
  
  $form['sso_cookie_base_admin_token'] = array(
    '#title' => t('Cookie admin token'),
    '#description' => t('Set sso cookie admin token defined in cs_cart'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => variable_get('sso_cookie_base_admin_token', 'sid_customer_'),
  );
  
  $form['sso_logout'] = array(
    '#title' => t('Cookie admin token'),
    '#description' => t('Set sso cookie admin token defined in cs_cart'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => variable_get('sso_logout', 'user/logout'),
  );
  
  $form['sso_roles'] = array(
    '#title' => t('User role'),
    '#description' => t('User role binded to sso users'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => variable_get('sso_roles', 'eshopuser'),
  );

  return system_settings_form($form);
}